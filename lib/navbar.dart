import 'package:sunsports/about.dart';
import 'package:sunsports/colorPick.dart';
import 'package:sunsports/home.dart';
import 'package:sunsports/produk.dart';
import 'package:flutter/material.dart';
import 'package:sunsports/profile.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';
  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [new BerandaPage(), Produk(), About(), Profile()];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.home_work,
            color: Colors.grey,
          ),
          title: new Text(
            'Home',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.post_add,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.post_add,
            color: Colors.grey,
          ),
          title: new Text('Tambah Produk'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home_work,
            color: Warna.biruMuda,
          ),
          icon: new Icon(
            Icons.account_circle_sharp,
            color: Colors.grey,
          ),
          title: new Text(
            'profile',
          ),
        ),
      ],
    );
  }
}
