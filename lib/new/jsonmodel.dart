import 'dart:convert';

List userModelFromJson(String str) => List.from(json.decode(str));

String userpostModelToJson(UserpostModel data) => json.encode(data.toJson());

class UserpostModel {
  UserpostModel({
    this.nama,
    this.harga,
    this.description,
    this.photoUrl,
  });

  String nama;
  String harga;
  String description;
  String photoUrl;

  Map toJson() => {
        "nama_produk": nama,
        "harga_produk": harga,
        "description_produk": description,
        "photo_url_produk": photoUrl,
      };
}
