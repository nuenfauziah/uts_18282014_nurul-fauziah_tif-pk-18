import 'package:sunsports/appBar.dart';
import 'package:sunsports/colorPick.dart';
import 'package:flutter/material.dart';

import 'new/viewmodel.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    getDataUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        appBar: AllAppBar(),
        body: _viewList(),
        backgroundColor: Colors.amber[100],
      ),
    );
  }

  Widget _viewList() {
    return ListView.builder(
      itemCount: dataUser.length,
      itemBuilder: (context, i) {
        return dataUser == null
            ? Container(child: Center(child: CircularProgressIndicator()))
            : InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return new AlertDialog(
                          backgroundColor: Colors.amber[100],
                          title: Container(
                            padding: EdgeInsets.all(5.0),
                            color: Colors.amberAccent,
                            child: Text(
                              'Deskripsi Produk',
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                          content: SingleChildScrollView(
                            child: ListBody(
                              children: [Text(dataUser[i]['description'])],
                            ),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text('Ok'))
                          ],
                        );
                      });
                },
                child: Container(
                  margin: EdgeInsets.all(5.0),
                  width: MediaQuery.of(context).size.width,
                  color: Color(0xff746C70),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(4),
                            child: Image.network(
                              dataUser[i]['photoUrl'],
                              fit: BoxFit.cover,
                              width: 100,
                              height: 100,
                            )),
                      ),
                      Container(
                        padding: EdgeInsets.all(5),
                        height: 100,
                        margin: EdgeInsets.only(left: 8),
                        child: Column(
                          children: [
                            Text(
                              dataUser[i]['nama'],
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              dataUser[i]['harga'],
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            Padding(
                              padding: EdgeInsets.all(8),
                            ),
                            Row(
                              children: [
                                Icon(
                                  Icons.accessibility_new_rounded,
                                  color: Colors.lightBlueAccent,
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  "Toko Alat Olahraga Terlengkap",
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.amberAccent,
                                      backgroundColor: Colors.transparent),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      // Container(
                      //   alignment: Alignment.bottomRight,
                      //   padding: EdgeInsets.only(left: 20, bottom: 10),
                      //   height: 100,
                      //   child: Text(
                      //     dataUser[i]['harga'],
                      //     style: TextStyle(
                      //         fontSize: 15,
                      //         fontWeight: FontWeight.bold,
                      //         color: Colors.white),
                      //   ),
                      // )
                    ],
                  ),
                ),
              );
      },
    );
  }
}
